
-- SUMMARY --

SMTP with elysia cron module allow you to send smtp mail in queue 
through elysia cron instead of default global cron. So you can set 
time by admin UI using elysia cron.It process SMTP queue with elysia cron.

SMTP with elysia cron provides users the ability to:

* Extend smtp module with elysia cron.
* Configure cron job execution time using elysia cron settings.


-- REQUIREMENTS --

1. SMTP
2. Elysia cron


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

None.


-- CUSTOMIZATION --

None.


--FUTURE PLANS--

* Expecting feature requests from community.
* Porting this module to Drupal 8.
